package com.DS2020.MedicationPlatform.consumer;

import com.DS2020.MedicationPlatform.dtos.Activity;
import com.DS2020.MedicationPlatform.dtos.AlertDto;
import com.DS2020.MedicationPlatform.entities.Patient;
import com.DS2020.MedicationPlatform.entities.SensorRecord;
import com.DS2020.MedicationPlatform.repositories.PatientRepository;
import com.DS2020.MedicationPlatform.repositories.SensorRecordRepository;
import com.DS2020.MedicationPlatform.services.AlertService;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.TimeUnit;

@Component
@AllArgsConstructor
public class Consumer implements MessageListener {

    private final SensorRecordRepository sensorRecordRepository;
    private final AlertService alertService;
    private final PatientRepository patientRepository;

    @Override

    // consumer is listening to this queue and, if a message is found there, it consumes it by printing the message
    @RabbitListener(queues = "${rabbitmq.queue}")
    public void onMessage(Message message) {
        ObjectMapper objectMapper = new ObjectMapper();
        Activity activity = null;
        try {
            activity = objectMapper.readValue(message.getBody(), Activity.class);
            System.out.println(activity);

            Patient patient = patientRepository.findById(activity.getPatient_id()).get();
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
            AlertDto alertDto = new AlertDto();
            alertDto.setCaregiverId(patient.getCaregiver().getId());

            switch (checkRules(activity)) {
                case -1:
                    alertDto.setMsg("Patient "+patient.getName()+" has slept more than 7 hours. Check their well-being. Received at "+ dtf.format(LocalDateTime.now()));
                    alertService.send(alertDto);
                    break;
                case -2:
                    alertDto.setMsg("Patient "+patient.getName()+" has left more than 5 hours ago. Check their well-being. Received at "+ dtf.format(LocalDateTime.now()));
                    alertService.send(alertDto);
                    break;
                case -3:
                    alertDto.setMsg("Patient "+patient.getName()+" has spent more than 30 minutes in the bathroom. Check their well-being. Received at "+ dtf.format(LocalDateTime.now()));
                    alertService.send(alertDto);
                    break;
                default:
                    break;
            }

            SensorRecord sensorRecord = new SensorRecord();
            sensorRecord.setPatient(patient);
            sensorRecord.setActivity(activity.getActivity());
            sensorRecord.setStartDate(activity.getStart());
            sensorRecord.setEndDate(activity.getEnd());
            sensorRecordRepository.save(sensorRecord);

            //TimeUnit.SECONDS.sleep(1);
            TimeUnit.MILLISECONDS.sleep(500);
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    public int checkRules(Activity activity) {
        if (activity.getActivity().equals("Sleeping") && (float) (activity.getEnd()-activity.getStart())/3600 > 7.0) {
            System.out.println("\nwake up!\n");
            return -1;
        }
        if (activity.getActivity().equals("Leaving") && (float) (activity.getEnd()-activity.getStart())/3600 > 5.0) {
            System.out.println("\ncome back!\n");
            return -2;
        }
        if (activity.getActivity().equals("Toileting") && (float) (activity.getEnd()-activity.getStart())/60 > 30.0) {
            System.out.println("\ndid you fall down the toilet?\n");
            return -3;
        }
        return 0;
    }
}
