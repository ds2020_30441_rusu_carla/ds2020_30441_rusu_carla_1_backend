package com.DS2020.MedicationPlatform.controllers;

import com.DS2020.MedicationPlatform.dtos.TokenDto;
import com.DS2020.MedicationPlatform.dtos.UserDto;
import com.DS2020.MedicationPlatform.services.AuthenticationService;
import com.DS2020.MedicationPlatform.sessions.SessionManager;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/authenticate")
@AllArgsConstructor
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class AuthenticationController {

    private final AuthenticationService authenticationService;

    @GetMapping(value = "/logout")
    public ResponseEntity logout(@RequestHeader("token") String token) {
        if (!SessionManager.getSessionMap().containsKey(token))
            return new ResponseEntity( HttpStatus.INTERNAL_SERVER_ERROR);
        else {
            authenticationService.logout(token);
            System.out.println("asfre");
            return new ResponseEntity( HttpStatus.OK);
        }
    }

    @PostMapping(value = "/login")
    public ResponseEntity<TokenDto> login(@RequestBody UserDto userDto) {
        TokenDto tokenDto = authenticationService.login(userDto);
        if (tokenDto != null)
            return new ResponseEntity<>(tokenDto, HttpStatus.OK);
        else
            return new ResponseEntity<>(tokenDto, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
