package com.DS2020.MedicationPlatform.controllers;

import com.DS2020.MedicationPlatform.dtos.CaregiverDto;
import com.DS2020.MedicationPlatform.dtos.CaregiverDto;
import com.DS2020.MedicationPlatform.services.CaregiverService;
import com.DS2020.MedicationPlatform.sessions.RequestValidator;
import com.DS2020.MedicationPlatform.sessions.Session;
import com.DS2020.MedicationPlatform.sessions.SessionManager;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/caregiver")
@AllArgsConstructor
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class CaregiverController {

    private final CaregiverService caregiverService;

    @PostMapping("/create")
    public ResponseEntity createCaregiver(@RequestBody CaregiverDto caregiverDto, @RequestHeader("token") String token) {
        Session session = SessionManager.getSessionMap().get(token);
        if (session == null) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        } else {
            if (!RequestValidator.validateDoctor(session))
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            caregiverService.create(caregiverDto);
            return ResponseEntity.ok().build();
        }
    }

    @GetMapping("/get-all")
    public ResponseEntity getAll(@RequestHeader("token") String token) {
        Session session = SessionManager.getSessionMap().get(token);
        if (session == null) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        } else {
            if (!RequestValidator.validateDoctor(session))
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            return new ResponseEntity(caregiverService.getAll(), HttpStatus.OK);
        }
    }

    @PostMapping("/update")
    public ResponseEntity updateCaregiver(@RequestBody CaregiverDto caregiverDto, @RequestHeader("token") String token) {
        Session session = SessionManager.getSessionMap().get(token);
        if (session == null) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        } else {
            if (!RequestValidator.validateDoctor(session))
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            caregiverService.update(caregiverDto);
            return ResponseEntity.ok().build();
        }
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity deleteCaregiver(@PathVariable("id") Long id, @RequestHeader("token") String token){
        Session session = SessionManager.getSessionMap().get(token);
        if (session == null) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        } else {
            if (!RequestValidator.validateDoctor(session))
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            caregiverService.delete(id);
            return ResponseEntity.ok().build();
        }
    }

    @GetMapping("/get-caregiver")
    public ResponseEntity getCaregiver(@RequestHeader("token") String token) {
        Session session = SessionManager.getSessionMap().get(token);
        if (session == null) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        } else {
            if (!RequestValidator.validateCaregiver(session))
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            return new ResponseEntity(caregiverService.getCaregiver(session.getUsername()), HttpStatus.OK);
        }
    }
}
