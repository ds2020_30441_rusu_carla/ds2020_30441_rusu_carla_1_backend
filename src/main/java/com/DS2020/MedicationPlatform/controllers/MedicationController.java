package com.DS2020.MedicationPlatform.controllers;

import com.DS2020.MedicationPlatform.dtos.MedicationDto;
import com.DS2020.MedicationPlatform.services.MedicationService;
import com.DS2020.MedicationPlatform.sessions.RequestValidator;
import com.DS2020.MedicationPlatform.sessions.Session;
import com.DS2020.MedicationPlatform.sessions.SessionManager;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/medication")
@AllArgsConstructor
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class MedicationController {

    private final MedicationService medicationService;

    @PostMapping("/create")
    public ResponseEntity createMedication(@RequestBody MedicationDto medicationDto, @RequestHeader("token") String token) {
        Session session = SessionManager.getSessionMap().get(token);
        if (session == null) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        } else {
            if (!RequestValidator.validateDoctor(session))
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            medicationService.create(medicationDto);
            return ResponseEntity.ok().build();
        }
    }

    @GetMapping("/get-all")
    public ResponseEntity getAll(@RequestHeader("token") String token) {
        Session session = SessionManager.getSessionMap().get(token);
        if (session == null) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        } else {
            if (!RequestValidator.validateDoctor(session))
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            return new ResponseEntity(medicationService.getAll(), HttpStatus.OK);
        }
    }

    @PostMapping("/update")
    public ResponseEntity updateMedication(@RequestBody MedicationDto medicationDto, @RequestHeader("token") String token) {
        Session session = SessionManager.getSessionMap().get(token);
        if (session == null) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        } else {
            if (!RequestValidator.validateDoctor(session))
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            medicationService.update(medicationDto);
            return ResponseEntity.ok().build();
        }
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity deleteMedication(@PathVariable("id") Long id, @RequestHeader("token") String token){
        Session session = SessionManager.getSessionMap().get(token);
        if (session == null) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        } else {
            if (!RequestValidator.validateDoctor(session))
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            medicationService.delete(id);
            return ResponseEntity.ok().build();
        }
    }
}
