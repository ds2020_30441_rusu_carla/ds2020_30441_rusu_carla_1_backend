package com.DS2020.MedicationPlatform.controllers;

import com.DS2020.MedicationPlatform.dtos.MedicationPlanDto;
import com.DS2020.MedicationPlatform.dtos.PatientDto;
import com.DS2020.MedicationPlatform.services.MedicationPlanService;
import com.DS2020.MedicationPlatform.sessions.RequestValidator;
import com.DS2020.MedicationPlatform.sessions.Session;
import com.DS2020.MedicationPlatform.sessions.SessionManager;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/plan")
@AllArgsConstructor
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class MedicationPlanController {

    private final MedicationPlanService medicationPlanService;

    @PostMapping("/create")
    public ResponseEntity createPlan(@RequestBody MedicationPlanDto medicationPlanDto, @RequestHeader("token") String token) {
        Session session = SessionManager.getSessionMap().get(token);
        if (session == null) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        } else {
            if (!RequestValidator.validateDoctor(session))
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            medicationPlanService.create(medicationPlanDto);
            return ResponseEntity.ok().build();
        }
    }

    @PostMapping("/by-patient")
    public ResponseEntity getByPatient(@RequestBody PatientDto PatientDto, @RequestHeader("token") String token) {
        return new ResponseEntity(medicationPlanService.getAllByPatient(PatientDto), HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity deletePlan(@PathVariable("id") Long id, @RequestHeader("token") String token){
        Session session = SessionManager.getSessionMap().get(token);
        if (session == null) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        } else {
            if (!RequestValidator.validateDoctor(session))
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            medicationPlanService.delete(id);
            return ResponseEntity.ok().build();
        }
    }
}
