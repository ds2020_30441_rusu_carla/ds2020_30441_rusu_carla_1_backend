package com.DS2020.MedicationPlatform.controllers;

import com.DS2020.MedicationPlatform.dtos.CaregiverDto;
import com.DS2020.MedicationPlatform.dtos.PatientDto;
import com.DS2020.MedicationPlatform.services.PatientService;
import com.DS2020.MedicationPlatform.sessions.RequestValidator;
import com.DS2020.MedicationPlatform.sessions.Session;
import com.DS2020.MedicationPlatform.sessions.SessionManager;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/patient")
@AllArgsConstructor
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class PatientController {

    private final PatientService patientService;

    @PostMapping("/create")
    public ResponseEntity createPatient(@RequestBody PatientDto patientDto, @RequestHeader("token") String token) {
        Session session = SessionManager.getSessionMap().get(token);
        if (session == null) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        } else {
            if (!RequestValidator.validateDoctor(session))
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            patientService.create(patientDto);
            return ResponseEntity.ok().build();
        }
    }

    @GetMapping("/get-all")
    public ResponseEntity getAll(@RequestHeader("token") String token) {
        Session session = SessionManager.getSessionMap().get(token);
        if (session == null) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        } else {
            if (!RequestValidator.validateDoctor(session))
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            return new ResponseEntity(patientService.getAll(), HttpStatus.OK);
        }
    }

    @GetMapping("/by-id")
    public ResponseEntity getById(@RequestHeader("token") String token) {
        Session session = SessionManager.getSessionMap().get(token);
        if (session == null) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        } else {
            if (!RequestValidator.validatePatient(session))
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            return new ResponseEntity(patientService.getById(session.getUsername()), HttpStatus.OK);
        }
    }

    @GetMapping("/by-caregiver")
    public ResponseEntity getByCaregiver(@RequestHeader("token") String token) {
        Session session = SessionManager.getSessionMap().get(token);
        if (session == null) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        } else {
            if (!RequestValidator.validateCaregiver(session))
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            return new ResponseEntity(patientService.getAllByCaregiver(session.getUsername()), HttpStatus.OK);

        }
    }

    @PostMapping("/update")
    public ResponseEntity updatePatient(@RequestBody PatientDto patientDto, @RequestHeader("token") String token) {
        Session session = SessionManager.getSessionMap().get(token);
        if (session == null) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        } else {
            if (!RequestValidator.validateDoctor(session))
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            patientService.update(patientDto);
            return ResponseEntity.ok().build();
        }
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity deletePatient(@PathVariable("id") Long id, @RequestHeader("token") String token){
        Session session = SessionManager.getSessionMap().get(token);
        if (session == null) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        } else {
            if (!RequestValidator.validateDoctor(session))
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            patientService.delete(id);
            return ResponseEntity.ok().build();
        }
    }

}
