package com.DS2020.MedicationPlatform.dtos;

import lombok.Data;

@Data
public class AlertDto {
    private Long caregiverId;
    private String msg;
}
