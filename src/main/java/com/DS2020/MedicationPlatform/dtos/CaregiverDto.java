package com.DS2020.MedicationPlatform.dtos;

import lombok.Data;
import java.sql.Timestamp;
import java.util.List;

@Data
public class CaregiverDto {
    private Long id;

    private UserDto user;

    private String name;

    private Timestamp dob;

    private String gender;

    private String address;

    private List<SimplePatientDto> patients;
}
