package com.DS2020.MedicationPlatform.dtos;

import lombok.Data;

@Data
public class MedicationDistributionDto {
    private Long id;

    private MedicationDto medication;

    private MedicationPlanDto medicationPlan;

    private String intakeInterval;
}
