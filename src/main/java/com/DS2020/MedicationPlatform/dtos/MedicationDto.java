package com.DS2020.MedicationPlatform.dtos;

import lombok.Data;

@Data
public class MedicationDto {
    private Long id;

    private String name;

    private String sideEffects;

    private String dosage;
}
