package com.DS2020.MedicationPlatform.dtos;

import com.DS2020.MedicationPlatform.entities.Patient;
import lombok.Data;

import java.util.List;

@Data
public class MedicationPlanDto {
    private Long id;

    private SimplePatientDto patient;

    private List<MedicationDistributionDto> medicationDistributions;

    private String treatmentPeriod;
}
