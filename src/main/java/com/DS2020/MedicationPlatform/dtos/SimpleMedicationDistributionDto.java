package com.DS2020.MedicationPlatform.dtos;

import lombok.Data;

@Data
public class SimpleMedicationDistributionDto {
    private Long id;

    private MedicationDto medication;

    private String intakeInterval;
}
