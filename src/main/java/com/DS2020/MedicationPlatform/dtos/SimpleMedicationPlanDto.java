package com.DS2020.MedicationPlatform.dtos;

import com.DS2020.MedicationPlatform.entities.Patient;
import lombok.Data;

import java.util.List;

@Data
public class SimpleMedicationPlanDto {
    private Long id;

    private SimplePatientDto patient;

    private List<SimpleMedicationDistributionDto> medicationDistributions;

    private String treatmentPeriod;
}
