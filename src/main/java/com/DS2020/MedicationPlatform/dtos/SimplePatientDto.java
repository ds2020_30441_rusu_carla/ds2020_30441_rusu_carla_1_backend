package com.DS2020.MedicationPlatform.dtos;

import lombok.Data;

import java.sql.Timestamp;

@Data
public class SimplePatientDto {
    private Long id;

    private UserDto user;

    private String name;

    private Timestamp dob;

    private String gender;

    private String address;

    private String medicalRecord;

    private SimpleCaregiverDto caregiver;
}
