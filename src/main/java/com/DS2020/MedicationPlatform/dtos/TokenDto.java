package com.DS2020.MedicationPlatform.dtos;

import lombok.Data;

@Data
public class TokenDto {

    private String token;
    private String type;
}
