package com.DS2020.MedicationPlatform.dtos;

import lombok.Data;

@Data
public class UserDto {
    private Long id;

    private String username;

    private String password;

    private String type;
}
