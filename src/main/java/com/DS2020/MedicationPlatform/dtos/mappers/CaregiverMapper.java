package com.DS2020.MedicationPlatform.dtos.mappers;

import com.DS2020.MedicationPlatform.dtos.CaregiverDto;
import com.DS2020.MedicationPlatform.dtos.SimpleCaregiverDto;
import com.DS2020.MedicationPlatform.entities.Caregiver;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "Spring")
public interface CaregiverMapper {

    CaregiverDto toDto(Caregiver caregiver);

    Caregiver toEntity(CaregiverDto caregiverDto);

    SimpleCaregiverDto toSimpleDto(Caregiver caregiver);

    Caregiver toSimpleEntity(SimpleCaregiverDto simpleCaregiverDto);
    
    List<CaregiverDto> toDtos(List<Caregiver> caregivers);

    List<Caregiver> toEntities(List<CaregiverDto> caregiverDtos);

    List<SimpleCaregiverDto> toSimpleDtos(List<Caregiver> caregivers);

    List<Caregiver> toSimpleEntities(List<SimpleCaregiverDto> caregiverDtos);
}
