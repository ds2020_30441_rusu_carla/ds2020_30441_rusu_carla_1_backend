package com.DS2020.MedicationPlatform.dtos.mappers;

import com.DS2020.MedicationPlatform.dtos.MedicationDistributionDto;
import com.DS2020.MedicationPlatform.dtos.SimpleMedicationDistributionDto;
import com.DS2020.MedicationPlatform.entities.MedicationDistribution;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "Spring")
public interface MedicationDistributionMapper {

    MedicationDistributionDto toDto(MedicationDistribution medicationDistribution);

    MedicationDistribution toEntity(MedicationDistributionDto medicationDistributionDto);

    SimpleMedicationDistributionDto toSimpleDto(MedicationDistribution medicationDistribution);

    MedicationDistribution toSimpleEntity(SimpleMedicationDistributionDto medicationDistributionDto);

    List<MedicationDistributionDto> toDtos(List<MedicationDistribution> medicationDistributions);

    List<MedicationDistribution> toEntities(List<MedicationDistributionDto>medicationDistributionDtos);

    List<SimpleMedicationDistributionDto> toSimpleDtos(List<MedicationDistribution> medicationDistributions);

    List<MedicationDistribution> toSimpleEntities(List<SimpleMedicationDistributionDto>medicationDistributionDtos);
}
