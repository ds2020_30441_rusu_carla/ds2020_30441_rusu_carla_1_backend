package com.DS2020.MedicationPlatform.dtos.mappers;

import com.DS2020.MedicationPlatform.dtos.MedicationDto;
import com.DS2020.MedicationPlatform.entities.Medication;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "Spring")
public interface MedicationMapper {

    MedicationDto toDto(Medication medication);

    Medication toEntity(MedicationDto medicationDto);

    List<MedicationDto> toDtos(List<Medication> medications);

    List<Medication> toEntities(List<MedicationDto> medicationDtos);
}
