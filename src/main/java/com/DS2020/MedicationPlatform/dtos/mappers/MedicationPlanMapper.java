package com.DS2020.MedicationPlatform.dtos.mappers;

import com.DS2020.MedicationPlatform.dtos.MedicationPlanDto;
import com.DS2020.MedicationPlatform.dtos.SimpleMedicationPlanDto;
import com.DS2020.MedicationPlatform.entities.MedicationPlan;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "Spring")
public interface MedicationPlanMapper {

    MedicationPlanDto toDto(MedicationPlan medicationPlan);

    MedicationPlan toEntity(MedicationPlanDto medicationPlanDto);

    SimpleMedicationPlanDto toSimpleDto(MedicationPlan medicationPlan);

    MedicationPlan toSimpleEntity(SimpleMedicationPlanDto medicationPlanDto);

    List<MedicationPlanDto> toDtos(List<MedicationPlan> medicationPlans);

    List<MedicationPlan> toEntities(List<MedicationPlanDto> medicationPlanDtos);

    List<SimpleMedicationPlanDto> toSimpleDtos(List<MedicationPlan> medicationPlans);

    List<MedicationPlan> toSimpleEntities(List<SimpleMedicationPlanDto> medicationPlanDtos);
}
