package com.DS2020.MedicationPlatform.dtos.mappers;

import com.DS2020.MedicationPlatform.dtos.PatientDto;
import com.DS2020.MedicationPlatform.dtos.SimplePatientDto;
import com.DS2020.MedicationPlatform.entities.Patient;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "Spring")
public interface PatientMapper {

    PatientDto toDto(Patient patient);

    Patient toEntity(PatientDto patientDto);

    SimplePatientDto toSimpleDto(Patient patient);

    Patient toSimpleEntity(SimplePatientDto simplePatientDto);

    List<PatientDto> toDtos(List<Patient> patients);

    List<Patient> toEntities(List<PatientDto> patientDtos);

    List<SimplePatientDto> toSimpleDtos(List<Patient> patients);

    List<Patient> toSimpleEntities(List<SimplePatientDto> patientDtos);
}
