package com.DS2020.MedicationPlatform.dtos.mappers;

import com.DS2020.MedicationPlatform.dtos.UserDto;
import com.DS2020.MedicationPlatform.entities.User;
import org.mapstruct.Mapper;

@Mapper(componentModel = "Spring")
public interface UserMapper {

    UserDto toDto(User user);

    User toEntity(UserDto userDto);
}
