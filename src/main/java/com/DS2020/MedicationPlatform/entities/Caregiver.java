package com.DS2020.MedicationPlatform.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.ToString;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

@Entity
@Data
@Table(name = "caregiver")
public class Caregiver implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @OneToOne(
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private User user;

    @Column(name = "name")
    private String name;

    @Column(name = "dob")
    private Timestamp dob;

    @Column(name = "gender")
    private String gender;

    @Column(name = "address")
    private String address;

    @OneToMany(mappedBy = "caregiver",
            orphanRemoval = false,
            fetch = FetchType.EAGER
    )
    //@JsonIgnore
    //@JsonIgnoreProperties(value = {"caregiver"}, allowSetters=true, allowGetters = false)
    @ToString.Exclude
    private List<Patient> patients;
}
