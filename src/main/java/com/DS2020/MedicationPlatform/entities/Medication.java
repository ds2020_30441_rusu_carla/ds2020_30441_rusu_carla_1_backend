package com.DS2020.MedicationPlatform.entities;

import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@Table(name = "medication")
public class Medication {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "side_effects")
    private String sideEffects;

    @Column(name = "dosage")
    private String dosage;

    @OneToMany(mappedBy = "medication",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    @ToString.Exclude
    private List<MedicationDistribution> medicationDistributions;
}
