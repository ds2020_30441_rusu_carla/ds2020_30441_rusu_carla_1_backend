package com.DS2020.MedicationPlatform.entities;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "medication_distribution")
public class MedicationDistribution {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "medication")
    private Medication medication;

    @ManyToOne
    @JoinColumn(name = "medication_plan")
    private MedicationPlan medicationPlan;

    @Column(name = "intake_interval")
    private String intakeInterval;
}
