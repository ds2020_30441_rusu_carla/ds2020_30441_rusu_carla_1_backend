package com.DS2020.MedicationPlatform.entities;

import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@Table(name = "medication_plan")
public class MedicationPlan {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "patient")
    @ToString.Exclude
    private Patient patient;

    @OneToMany(mappedBy = "medicationPlan",
            cascade = CascadeType.ALL,
            orphanRemoval = true,
            fetch = FetchType.EAGER
    )
    @ToString.Exclude
    private List<MedicationDistribution> medicationDistributions;


    @Column(name = "treatment_period")
    private String treatmentPeriod;
}
