package com.DS2020.MedicationPlatform.entities;

import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

@Entity
@Data
@Table(name = "patient")
public class Patient implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @OneToOne(
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private User user;

    @Column(name = "name")
    private String name;

    @Column(name = "dob")
    private Timestamp dob;

    @Column(name = "gender")
    private String gender;

    @Column(name = "address")
    private String address;

    @Column(name = "medical_record")
    private String medicalRecord;

    @ManyToOne
    @JoinColumn(name = "caregiver")
    //@ToString.Exclude
    //@JsonIgnoreProperties(value = {"patients"}, allowSetters=true, allowGetters = false)
    private Caregiver caregiver;

    @OneToMany(mappedBy = "patient",
            cascade = CascadeType.ALL,
            orphanRemoval = true,
            fetch = FetchType.EAGER
    )
    @ToString.Exclude
    private List<MedicationPlan> medicationPlans;
}
