package com.DS2020.MedicationPlatform.repositories;

import com.DS2020.MedicationPlatform.entities.Caregiver;
import com.DS2020.MedicationPlatform.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface CaregiverRepository extends JpaRepository<Caregiver, Long> {

    Caregiver findByUser(User user);
}
