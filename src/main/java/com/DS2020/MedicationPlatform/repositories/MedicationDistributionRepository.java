package com.DS2020.MedicationPlatform.repositories;

import com.DS2020.MedicationPlatform.entities.MedicationDistribution;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MedicationDistributionRepository extends JpaRepository<MedicationDistribution, Long> {
}
