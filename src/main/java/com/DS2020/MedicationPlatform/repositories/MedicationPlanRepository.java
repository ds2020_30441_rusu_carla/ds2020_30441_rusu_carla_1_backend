package com.DS2020.MedicationPlatform.repositories;

import com.DS2020.MedicationPlatform.entities.MedicationPlan;
import com.DS2020.MedicationPlatform.entities.Patient;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

public interface MedicationPlanRepository extends JpaRepository<MedicationPlan, Long> {

    List<MedicationPlan> findAllByPatient(Patient patient);
}
