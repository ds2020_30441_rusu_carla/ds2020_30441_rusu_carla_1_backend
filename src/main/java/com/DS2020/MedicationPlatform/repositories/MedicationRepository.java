package com.DS2020.MedicationPlatform.repositories;

import com.DS2020.MedicationPlatform.entities.Medication;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface MedicationRepository extends JpaRepository<Medication, Long> {
}
