package com.DS2020.MedicationPlatform.repositories;

import com.DS2020.MedicationPlatform.entities.Caregiver;
import com.DS2020.MedicationPlatform.entities.Patient;
import com.DS2020.MedicationPlatform.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface PatientRepository extends JpaRepository<Patient, Long> {

    Optional<Patient> findById(Long id);

    List<Patient> findAllByCaregiver(Caregiver caregiver);

    Patient findByUser(User user);
}
