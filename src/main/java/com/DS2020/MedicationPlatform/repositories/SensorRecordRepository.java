package com.DS2020.MedicationPlatform.repositories;

import com.DS2020.MedicationPlatform.entities.SensorRecord;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SensorRecordRepository extends JpaRepository<SensorRecord, Long> {
}
