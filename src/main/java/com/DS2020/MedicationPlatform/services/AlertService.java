package com.DS2020.MedicationPlatform.services;

import com.DS2020.MedicationPlatform.dtos.Activity;
import com.DS2020.MedicationPlatform.dtos.AlertDto;

public interface AlertService {
    void send(AlertDto alertDto);
}
