package com.DS2020.MedicationPlatform.services;

import com.DS2020.MedicationPlatform.dtos.AlertDto;
import lombok.AllArgsConstructor;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class AlertServiceImpl implements AlertService{

    private final SimpMessagingTemplate simpMessagingTemplate;
    private  static final String WS_MESSAGE_TRANSFER_DESTINATION = "/alert";

    @Override
    public void send(AlertDto alertDto) {
        simpMessagingTemplate.convertAndSend(WS_MESSAGE_TRANSFER_DESTINATION, alertDto);
    }

}
