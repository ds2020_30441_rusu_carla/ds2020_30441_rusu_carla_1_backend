package com.DS2020.MedicationPlatform.services;

import com.DS2020.MedicationPlatform.dtos.TokenDto;
import com.DS2020.MedicationPlatform.dtos.UserDto;

public interface AuthenticationService {
    TokenDto login(UserDto userDto);
    void logout(String token);
}
