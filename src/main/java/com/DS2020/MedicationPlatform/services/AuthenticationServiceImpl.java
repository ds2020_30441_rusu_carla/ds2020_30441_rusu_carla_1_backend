package com.DS2020.MedicationPlatform.services;

import com.DS2020.MedicationPlatform.dtos.TokenDto;
import com.DS2020.MedicationPlatform.dtos.UserDto;
import com.DS2020.MedicationPlatform.entities.User;
import com.DS2020.MedicationPlatform.repositories.UserRepository;
import com.DS2020.MedicationPlatform.sessions.Session;
import com.DS2020.MedicationPlatform.sessions.SessionManager;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.Instant;

@Service
@AllArgsConstructor
public class AuthenticationServiceImpl implements AuthenticationService{

    private final UserRepository userRepository;

    @Override
    public TokenDto login(UserDto userDto) {
        User user = userRepository.findByUsername(userDto.getUsername());

        if (user != null) {
            if (user.getPassword().equals(userDto.getPassword())) {
                Session session = new Session(userDto.getUsername(), Instant.now(), Session.EXPIRATION_TIME, user.getType());
                String token = SessionManager.add(session);

                TokenDto tokenDto = new TokenDto();
                tokenDto.setToken(token);
                tokenDto.setType(user.getType());
                return tokenDto;
            } else
                return null;
        } else
            return null;
    }

    @Override
    public void logout(String token) {
        SessionManager.getSessionMap().remove(token);
    }
}
