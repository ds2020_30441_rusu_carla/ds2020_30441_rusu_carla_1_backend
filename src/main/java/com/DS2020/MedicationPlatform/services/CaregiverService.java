package com.DS2020.MedicationPlatform.services;

import com.DS2020.MedicationPlatform.dtos.CaregiverDto;
import com.DS2020.MedicationPlatform.dtos.SimpleCaregiverDto;

import java.util.List;

public interface CaregiverService {
    void create(CaregiverDto caregiverDto);
    List<CaregiverDto> getAll();
    void update(CaregiverDto caregiverDto);
    void delete(Long id);
    Long getCaregiver(String token);

}
