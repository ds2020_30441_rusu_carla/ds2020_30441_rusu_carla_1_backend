package com.DS2020.MedicationPlatform.services;

import com.DS2020.MedicationPlatform.dtos.CaregiverDto;
import com.DS2020.MedicationPlatform.dtos.SimpleCaregiverDto;
import com.DS2020.MedicationPlatform.dtos.mappers.CaregiverMapper;
import com.DS2020.MedicationPlatform.dtos.mappers.PatientMapper;
import com.DS2020.MedicationPlatform.entities.Caregiver;
import com.DS2020.MedicationPlatform.entities.Patient;
import com.DS2020.MedicationPlatform.entities.User;
import com.DS2020.MedicationPlatform.repositories.CaregiverRepository;
import com.DS2020.MedicationPlatform.repositories.PatientRepository;
import com.DS2020.MedicationPlatform.repositories.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class CaregiverServiceImpl implements CaregiverService{

    private final CaregiverRepository caregiverRepository;
    private final CaregiverMapper caregiverMapper;

    private final PatientRepository patientRepository;
    private final PatientMapper patientMapper;

    private final UserRepository userRepository;

    @Override
    @Transactional
    public void create(CaregiverDto caregiverDto) {
        Caregiver caregiver = caregiverMapper.toEntity(caregiverDto);
        caregiver.getUser().setType("caregiver");

        if (caregiver.getPatients()!=null) {
            List<Patient> patients = caregiver.getPatients();
            patients = patients.stream().map(patient -> patient = patientRepository.findById(patient.getId()).get()).collect(Collectors.toList());

            caregiver.setPatients(patients);
            patients.forEach(patient -> patient.setCaregiver(caregiver));
        }

        caregiverRepository.save(caregiver);
    }

    @Override
    public List<CaregiverDto> getAll() {
        return caregiverMapper.toDtos(caregiverRepository.findAll());
    }

    @Override
    @Transactional
    public void update(CaregiverDto caregiverDto) {
        Caregiver caregiver = caregiverRepository.findById(caregiverDto.getId()).get();

        if (caregiverDto.getName() != null)                 caregiver.setName(caregiverDto.getName());
        if (caregiverDto.getDob() != null)                  caregiver.setDob(caregiverDto.getDob());
        if (caregiverDto.getGender() != null)               caregiver.setGender(caregiverDto.getGender());
        if (caregiverDto.getAddress() != null)              caregiver.setAddress(caregiverDto.getAddress());

        if (caregiverDto.getUser() != null) {
            if ((caregiverDto.getUser().getPassword() != null))
                caregiver.getUser().setPassword(caregiverDto.getUser().getPassword());
            if ((caregiverDto.getUser().getUsername() != null))
                caregiver.getUser().setUsername(caregiverDto.getUser().getUsername());
        }

        if (caregiverDto.getPatients() != null) {
            caregiverRepository.findById(caregiver.getId()).get().getPatients().forEach(patient -> patient.setCaregiver(null));

            List<Patient> patients = patientMapper.toSimpleEntities(caregiverDto.getPatients());
            patients = patients.stream().map(patient -> patient = patientRepository.findById(patient.getId()).get()).collect(Collectors.toList());

            caregiver.setPatients(patients);
            patients.forEach(patient -> patient.setCaregiver(caregiver));
        }

        caregiverRepository.save(caregiver);
    }

    @Override
    @Transactional
    public void delete(Long id) {
        caregiverRepository.findById(id).get().getPatients().forEach(patient -> patient.setCaregiver(null));
        caregiverRepository.deleteById(id);
    }

    @Override
    public Long getCaregiver(String username) {
        User user = userRepository.findByUsername(username);
        return caregiverRepository.findByUser(user).getId();
    }
}
