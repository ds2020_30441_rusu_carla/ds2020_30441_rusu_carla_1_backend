package com.DS2020.MedicationPlatform.services;

import com.DS2020.MedicationPlatform.dtos.MedicationPlanDto;
import com.DS2020.MedicationPlatform.dtos.PatientDto;
import com.DS2020.MedicationPlatform.dtos.SimpleMedicationPlanDto;

import java.util.List;

public interface MedicationPlanService {
    void create(MedicationPlanDto medicationPlanDto);
    List<MedicationPlanDto> getAll();
    List<SimpleMedicationPlanDto> getAllByPatient(PatientDto patientDto);
    void update(MedicationPlanDto medicationPlanDto);
    void delete(Long id);
}
