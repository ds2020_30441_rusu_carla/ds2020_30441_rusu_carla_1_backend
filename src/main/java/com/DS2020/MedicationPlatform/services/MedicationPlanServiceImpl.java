package com.DS2020.MedicationPlatform.services;

import com.DS2020.MedicationPlatform.dtos.MedicationPlanDto;
import com.DS2020.MedicationPlatform.dtos.PatientDto;
import com.DS2020.MedicationPlatform.dtos.SimpleMedicationPlanDto;
import com.DS2020.MedicationPlatform.dtos.mappers.MedicationPlanMapper;
import com.DS2020.MedicationPlatform.entities.MedicationDistribution;
import com.DS2020.MedicationPlatform.entities.MedicationPlan;
import com.DS2020.MedicationPlatform.entities.Patient;
import com.DS2020.MedicationPlatform.repositories.MedicationDistributionRepository;
import com.DS2020.MedicationPlatform.repositories.MedicationPlanRepository;
import com.DS2020.MedicationPlatform.repositories.MedicationRepository;
import com.DS2020.MedicationPlatform.repositories.PatientRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@AllArgsConstructor
public class MedicationPlanServiceImpl implements MedicationPlanService{

    private final MedicationPlanRepository medicationPlanRepository;
    private final MedicationPlanMapper medicationPlanMapper;

    private final PatientRepository patientRepository;
    private final MedicationRepository medicationRepository;

    @Override
    @Transactional
    public void create(MedicationPlanDto medicationPlanDto) {
        MedicationPlan medicationPlan = medicationPlanMapper.toEntity(medicationPlanDto);

        // find patient for whom we want to create a new plan
        Patient patient = patientRepository.findById(medicationPlan.getPatient().getId()).get();

        // set the patient foreign key in the medication plan
        medicationPlan.setPatient(patient);

        // get all medication distributions for the plan
        List<MedicationDistribution> medicationDistributions = medicationPlan.getMedicationDistributions();

        // set the medication plan for each distribution
        medicationDistributions.forEach(medicationDistribution -> medicationDistribution.setMedicationPlan(medicationPlan));

        System.out.println(medicationDistributions);

        // set the medication for each distribution
        medicationDistributions.forEach(medicationDistribution -> medicationDistribution.setMedication(medicationRepository.findById(medicationDistribution.getMedication().getId()).get()));

        medicationPlanRepository.save(medicationPlan);
    }

    @Override
    public List<MedicationPlanDto> getAll() {
        return null;
    }

    @Override
    public List<SimpleMedicationPlanDto> getAllByPatient(PatientDto patientDto) {
        if (patientRepository.findById(patientDto.getId()).isPresent()) {
            //List<MedicationPlan> medicationPlans = patientRepository.findById(patientDto.getId()).get().getMedicationPlans();
            List<MedicationPlan>medicationPlans = medicationPlanRepository.findAllByPatient(patientRepository.findById(patientDto.getId()).get());

            System.out.println(medicationPlans);

            List<SimpleMedicationPlanDto> simpleMedicationPlanDtos = medicationPlanMapper.toSimpleDtos(medicationPlans);
            return simpleMedicationPlanDtos;
        } else return null;
    }

    @Override
    public void update(MedicationPlanDto medicationPlanDto) {

    }

    @Override
    @Transactional
    public void delete(Long id) {
        MedicationPlan medicationPlan = medicationPlanRepository.findById(id).get();
        Patient patient = patientRepository.findById(medicationPlan.getPatient().getId()).get();

        patient.getMedicationPlans().remove(medicationPlan);
        medicationPlanRepository.delete(medicationPlan);
    }
}
