package com.DS2020.MedicationPlatform.services;

import com.DS2020.MedicationPlatform.dtos.MedicationDto;

import java.util.List;

public interface MedicationService {
    void create(MedicationDto medicationDto);
    List<MedicationDto> getAll();
    void update(MedicationDto medicationDto);
    void delete(Long id);
}
