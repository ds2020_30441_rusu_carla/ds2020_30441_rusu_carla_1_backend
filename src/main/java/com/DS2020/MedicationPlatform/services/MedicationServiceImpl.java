package com.DS2020.MedicationPlatform.services;

import com.DS2020.MedicationPlatform.dtos.MedicationDto;
import com.DS2020.MedicationPlatform.dtos.mappers.MedicationMapper;
import com.DS2020.MedicationPlatform.entities.Medication;
import com.DS2020.MedicationPlatform.repositories.MedicationRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class MedicationServiceImpl implements MedicationService{

    private final MedicationRepository medicationRepository;
    private final MedicationMapper medicationMapper;

    @Override
    public void create(MedicationDto medicationDto) {
        medicationRepository.save(medicationMapper.toEntity(medicationDto));
    }

    @Override
    public List<MedicationDto> getAll() {
        return medicationMapper.toDtos(medicationRepository.findAll());
    }

    @Override
    public void update(MedicationDto medicationDto) {
        Medication medication = medicationRepository.findById(medicationDto.getId()).get();

        if (medicationDto.getName() != null)                 medication.setName(medicationDto.getName());
        if (medicationDto.getSideEffects() != null)          medication.setSideEffects(medicationDto.getSideEffects());
        if (medicationDto.getDosage() != null)               medication.setDosage(medicationDto.getDosage());

        medicationRepository.save(medication);
    }

    @Override
    public void delete(Long id) {
        medicationRepository.deleteById(id);
    }
}
