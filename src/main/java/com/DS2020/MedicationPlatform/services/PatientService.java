package com.DS2020.MedicationPlatform.services;

import com.DS2020.MedicationPlatform.dtos.CaregiverDto;
import com.DS2020.MedicationPlatform.dtos.PatientDto;
import com.DS2020.MedicationPlatform.dtos.SimplePatientDto;

import java.util.List;

public interface PatientService {
    void create(PatientDto patientDto);
    List<SimplePatientDto> getAll();
    List<SimplePatientDto> getAllByCaregiver(String username);
    SimplePatientDto getById(String username);
    void update(PatientDto patientDto);
    void delete(Long id);
}
