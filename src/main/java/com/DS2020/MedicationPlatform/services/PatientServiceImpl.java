package com.DS2020.MedicationPlatform.services;

import com.DS2020.MedicationPlatform.dtos.CaregiverDto;
import com.DS2020.MedicationPlatform.dtos.PatientDto;
import com.DS2020.MedicationPlatform.dtos.SimplePatientDto;
import com.DS2020.MedicationPlatform.dtos.mappers.CaregiverMapper;
import com.DS2020.MedicationPlatform.dtos.mappers.PatientMapper;
import com.DS2020.MedicationPlatform.entities.Caregiver;
import com.DS2020.MedicationPlatform.entities.Patient;
import com.DS2020.MedicationPlatform.entities.User;
import com.DS2020.MedicationPlatform.repositories.CaregiverRepository;
import com.DS2020.MedicationPlatform.repositories.PatientRepository;
import com.DS2020.MedicationPlatform.repositories.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@AllArgsConstructor
public class PatientServiceImpl implements PatientService{

    private final PatientRepository patientRepository;
    private final PatientMapper patientMapper;

    private final CaregiverMapper caregiverMapper;
    private final CaregiverRepository caregiverRepository;
    private final UserRepository userRepository;

    @Override
    @Transactional
    public void create(PatientDto patientDto) {
        Patient patient = patientMapper.toEntity(patientDto);
        patient.getUser().setType("patient");

        if (patientDto.getCaregiver()!=null) {
            Caregiver caregiver = caregiverRepository.findById(patient.getCaregiver().getId()).get();

            patient.setCaregiver(caregiver);
        }

        patientRepository.save(patient);
    }

    @Override
    public List<SimplePatientDto> getAll() {
        return patientMapper.toSimpleDtos(patientRepository.findAll());
    }

    @Override
    public List<SimplePatientDto> getAllByCaregiver(String username) {
        User user = userRepository.findByUsername(username);
        List<SimplePatientDto> simplePatientDtos = patientMapper.toSimpleDtos(caregiverRepository.findByUser(user).getPatients());
        simplePatientDtos.forEach(simplePatientDto -> simplePatientDto.setCaregiver(null));

        return simplePatientDtos;
    }

    @Override
    public SimplePatientDto getById(String username) {
        User user = userRepository.findByUsername(username);
        return patientMapper.toSimpleDto(patientRepository.findByUser(user));
    }

    @Override
    @Transactional
    public void update(PatientDto patientDto) {
        Patient patient = patientRepository.findById(patientDto.getId()).get();

        if (patientDto.getName() != null)                 patient.setName(patientDto.getName());
        if (patientDto.getDob() != null)                  patient.setDob(patientDto.getDob());
        if (patientDto.getGender() != null)               patient.setGender(patientDto.getGender());
        if (patientDto.getAddress() != null)              patient.setAddress(patientDto.getAddress());
        if (patientDto.getMedicalRecord() != null)        patient.setMedicalRecord(patientDto.getMedicalRecord());

        if (patientDto.getUser() != null) {
            if ((patientDto.getUser().getPassword() != null))
                patient.getUser().setPassword(patientDto.getUser().getPassword());
            if ((patientDto.getUser().getUsername() != null))
                patient.getUser().setUsername(patientDto.getUser().getUsername());
        }

        if (patientDto.getCaregiver() != null) {
            patient.setCaregiver(null);
            if (patientDto.getCaregiver().getId() != null) {
                Caregiver caregiver = caregiverRepository.findById(patientDto.getCaregiver().getId()).get();

                patient.setCaregiver(caregiver);
            }
        }

        patientRepository.save(patient);
    }

    @Override
    public void delete(Long id) {
        patientRepository.deleteById(id);
    }
}
