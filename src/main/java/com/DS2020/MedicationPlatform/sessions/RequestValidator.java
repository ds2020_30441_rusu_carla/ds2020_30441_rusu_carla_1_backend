package com.DS2020.MedicationPlatform.sessions;

import java.time.Instant;
import java.time.temporal.ChronoUnit;

public class RequestValidator {
    public static boolean isDoctor(Session session){
        if (session.getType().equals("doctor"))
            return true;
        else
            return false;
    }

    public static boolean isPatient(Session session){
        if (session.getType().equals("patient"))
            return true;
        else
            return false;
    }

    public static boolean isCaregiver(Session session){
        if (session.getType().equals("caregiver"))
            return true;
        else
            return false;
    }

    public static boolean isExpired(Session session){
        System.out.println(session.getCreationTime());
        System.out.println(session.getExpirationPeriod().toHours());
        System.out.println(session.getCreationTime().plus(session.getExpirationPeriod().toHours(), ChronoUnit.HOURS));
        System.out.println(session.getCreationTime());
        System.out.println(session.getCreationTime().plus(session.getExpirationPeriod().toHours(), ChronoUnit.HOURS).compareTo(Instant.now()));
        if (session.getCreationTime().plus(session.getExpirationPeriod().toHours(), ChronoUnit.HOURS).compareTo(Instant.now()) < 0)
            return true;
        else
            return false;
    }

    public static boolean validateDoctor(Session session){
        if (RequestValidator.isDoctor(session) && !RequestValidator.isExpired(session))
            return true;
        System.out.println(RequestValidator.isDoctor(session) +"   "+ !RequestValidator.isExpired(session));
        return false;
    }

    public static boolean validatePatient(Session session){
        if (RequestValidator.isPatient(session) && !RequestValidator.isExpired(session))
            return true;
        System.out.println(RequestValidator.isPatient(session) +"   "+ !RequestValidator.isExpired(session));
        return false;
    }

    public static boolean validateCaregiver(Session session){
        if (RequestValidator.isCaregiver(session) && !RequestValidator.isExpired(session))
            return true;
        System.out.println(RequestValidator.isCaregiver(session) +"   "+ !RequestValidator.isExpired(session));
        return false;
    }

}
