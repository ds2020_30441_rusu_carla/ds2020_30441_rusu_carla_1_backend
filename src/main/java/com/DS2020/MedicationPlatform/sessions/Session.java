package com.DS2020.MedicationPlatform.sessions;

import java.time.Duration;
import java.time.Instant;


public class Session {
    public static final Duration EXPIRATION_TIME = Duration.ofHours(2);
    private String username;
    private Instant creationTime;
    private Duration expirationPeriod;
    private String type;

    public Session() {
    }

    public Session(String username, Instant creationTime, Duration expirationPeriod, String type) {
        this.username = username;
        this.creationTime = creationTime;
        this.expirationPeriod = expirationPeriod;
        this.type = type;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Instant getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Instant creationTime) {
        this.creationTime = creationTime;
    }

    public Duration getExpirationPeriod() {
        return expirationPeriod;
    }

    public void setExpirationPeriod(Duration expirationPeriod) {
        this.expirationPeriod = expirationPeriod;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Session{" +
                "username='" + username + '\'' +
                ", creationTime=" + creationTime +
                ", expirationPeriod=" + expirationPeriod +
                ", type=" + type +
                '}';
    }
}