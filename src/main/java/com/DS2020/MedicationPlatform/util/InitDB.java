package com.DS2020.MedicationPlatform.util;

import com.DS2020.MedicationPlatform.entities.Caregiver;
import com.DS2020.MedicationPlatform.entities.Patient;
import com.DS2020.MedicationPlatform.entities.User;
import com.DS2020.MedicationPlatform.repositories.CaregiverRepository;
import com.DS2020.MedicationPlatform.repositories.PatientRepository;
import com.DS2020.MedicationPlatform.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class InitDB {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private CaregiverRepository caregiverRepository;
    @Autowired
    private PatientRepository patientRepository;

    @PostConstruct
    public void init(){
        // initialize your monitor here, instance of someService is already injected by this time.
        if (userRepository.findByUsername("doctor") == null) {
            User user = new User();
            user.setUsername("doctor");
            user.setPassword("doctor");
            user.setType("doctor");
            userRepository.save(user);
        }
        if (userRepository.findByUsername("caregiver") == null) {
            User user = new User();
            user.setUsername("caregiver");
            user.setPassword("caregiver");
            user.setType("caregiver");

            Caregiver caregiver = new Caregiver();

            caregiver.setName("Diana Pop");
            caregiver.setAddress("5th Ave");
            caregiver.setGender("female");

            caregiver.setUser(user);

            userRepository.save(user);
            caregiverRepository.save(caregiver);
        }
        if (userRepository.findByUsername("patient") == null) {
            User user = new User();
            user.setUsername("patient");
            user.setPassword("patient");
            user.setType("patient");

            Patient patient = new Patient();

            patient.setName("Eugen Ion");
            patient.setAddress("89 H.B. Street");
            patient.setGender("male");
            patient.setMedicalRecord("Healthy");

            patient.setUser(user);

            Caregiver caregiver = caregiverRepository.findById(1L).get();
            patient.setCaregiver(caregiver);

            caregiver.getPatients().add(patient);

            userRepository.save(user);
            patientRepository.save(patient);
        }
    }
}
